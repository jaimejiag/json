package com.usuario.json;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.NetworkImageView;

import java.util.List;

/**
 * Created by usuario on 10/01/17.
 */

public class MyRecyclerAdapter extends RecyclerView.Adapter<MyRecyclerAdapter.MyViewHolder> {
    private List<NewsFeed> feedsList;
    private Context context;
    private LayoutInflater inflater;

    public MyRecyclerAdapter(Context context, List<NewsFeed> feedsList) {
        this.context = context;
        this.feedsList = feedsList;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = inflater.inflate(R.layout.single_item, parent, false);
        return new MyViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        NewsFeed feed = feedsList.get(position);
        //Pass the values of feeds object to Views
        holder.title.setText(feed.getTitle());
        holder.description.setText(feed.getDescription());
        holder.imageview.setImageUrl(feed.getImage(),
                NetworkController.getInstance(context).getImageLoader());
    }

    @Override
    public int getItemCount() {
        return feedsList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView title, description;
        private NetworkImageView imageview;

        public MyViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title_view);
            description = (TextView) itemView.findViewById(R.id.description_view);
            // Volley's NetworkImageView which will load Image from URL
            imageview = (NetworkImageView) itemView.findViewById(R.id.thumbnail_view);

            itemView.setClickable(true);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();

                    if (position != RecyclerView.NO_POSITION)
                        Toast.makeText(context, "Fecha: " + feedsList.get(position).getPubDate(), Toast.LENGTH_SHORT).show();


                }
            });

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    int position = getAdapterPosition();
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(feedsList.get(position).getLink()));

                    if (intent.resolveActivity(context.getPackageManager()) != null)
                        context.startActivity(intent);
                    else
                        Toast.makeText(context, "No hay navegador", Toast.LENGTH_SHORT).show();

                    return true;
                }
            });
        }
    }
}